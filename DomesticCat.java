public class DomesticCat extends Pet{
    public DomesticCat(Spacies species, String nickName) {
        super(species, nickName);
    }

    public DomesticCat(Spacies species, String nickName, int age, int trickLevel, String[] habits) {
        super(species, nickName, age, trickLevel, habits);
    }

    @Override
    public void eat() {

    }

    @Override
    public void respond() {

    }
}
