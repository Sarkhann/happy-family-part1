public class Main {
    public static void main(String[] args) {
        Man father = new Man("John", "Kennedy", 1917);
        Woman mother = new Woman("Jacqueline", "Kennedy", 1929);
        String[][] schedule = {{DayOfWeek.SUNDAY.name()}, {"Wake up", "Get up"}};
        Human child = new Human("Caroline", "Kennedy", (byte) 5, 1957, schedule);

        Family family = new Family(father, mother);

        child.setFamily(family);
        mother.setFamily(family);
        father.setFamily(family);


        Pet pet = new Fish( Spacies.DOG,"Hamid", 5, 35, new String[]{"fvbd"});
        String[] habits = {"eat", "drink", "sleep"};

        System.out.println(child);
        Human newChild = mother.bornChild();
        family.addChild(newChild);
        System.out.println(family);
}
}