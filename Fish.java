public class Fish extends Pet{


    public Fish(Spacies species, String nickName) {
        super(species, nickName);
    }

    public Fish(Spacies species, String nickName, int age, int trickLevel, String[] habits) {
        super(species, nickName, age, trickLevel, habits);
    }

    @Override
    public void eat() {

    }

    @Override
    public void respond() {

    }
}
