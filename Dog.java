public class Dog extends Pet{
    public Dog(Spacies species, String nickName) {
        super(species, nickName);
    }

    public Dog(Spacies species, String nickName, int age, int trickLevel, String[] habits) {
        super(species, nickName, age, trickLevel, habits);
    }

    @Override
    public void eat() {

    }

    @Override
    public void respond() {

    }
}
