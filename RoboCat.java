public class RoboCat extends Pet{
    public RoboCat(Spacies species, String nickName) {
        super(species, nickName);
    }

    public RoboCat(Spacies species, String nickName, int age, int trickLevel, String[] habits) {
        super(species, nickName, age, trickLevel, habits);
    }

    @Override
    public void eat() {

    }

    @Override
    public void respond() {

    }
}
