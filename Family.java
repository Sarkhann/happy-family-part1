import java.util.Random;

public class Family {
    private Human father;
    private Human mother;
    private Human[] children;
    private Pet pet;

    static {
        System.out.println(Family.class.getName() + " class is loaded");
    }

    {
        System.out.println(Family.class.getName() + " object is created");
    }


    public Family(Human father, Human mother) {
        this.father = father;
        this.mother = mother;
        children = new Human[0];
    }

    public void addChild(Human child) {
        Human[] newChildren = new Human[children.length + 1];
        for (int i = 0; i < children.length; i++) {
            newChildren[i] = children[i];
        }
        newChildren[newChildren.length - 1] = child;
        this.children = newChildren;
    }

    public boolean deleteChild(int index) {
        Human[] newChildren = new Human[children.length - 1];
        if (index >= children.length) {
            return false;
        } else {
            children[index] = null;
            int j = 0;
            for (int i = 0; i < children.length; i++) {
                if (children[i] != null) {
                    newChildren[j] = children[i];
                    j++;
                }
            }
            this.children = newChildren;
            return true;
        }
    }

    public boolean deleteChild(Human child) {
        Human[] newChildren = new Human[children.length - 1];
        int i;
        for (i = 0; i < children.length; i++) {
            if (children[i].equals(child)) {
                children[i] = null;
                break;
            }
        }
        if (i == children.length) {
            return false;
        } else {
            int j = 0;
            for (int k = 0; k < children.length; k++) {
                if (children[k] != null) {
                    newChildren[j] = children[k];
                    j++;
                }
            }
            this.children = newChildren;
            return true;
        }
    }

    public int countFamilyMembers() {
        if (this.pet != null) {
            return (children.length + 3);
        } else {
            return (children.length + 2);
        }
    }

    public boolean equals(Object object) {
        Family checkedFamily = ((Family) object);
        return checkedFamily.father.equals(this.father) && checkedFamily.mother.equals(this.mother);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (this.children != null) {
            for (int i = 0; i < this.children.length; i++) {
                sb.append(children[i].getName()).append(", ");
            }
            if (pet != null) {
                return "Family consists of " + this.countFamilyMembers() + " members.Father: " + this.father.getName() + " mother: " + this.mother.getName() + " children: " + sb + " pet: " + this.pet.getNickName();
            } else {
                return "Family consists of " + this.countFamilyMembers() + " members.Father: " + this.father.getName() + " mother: " + this.mother.getName() + " children: " + sb;
            }
        } else {
            return "Family consists of " + this.countFamilyMembers() + " members.Father: " + this.father.getName() + " mother: " + this.mother.getName();
        }
    }

    public int hashCode() {
        Random random = new Random();
        int hashCode = random.nextInt(1001);
        return hashCode;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }
}
