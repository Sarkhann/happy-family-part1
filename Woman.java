import java.util.Random;

public final class Woman extends Human implements HumanCreator {
    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, Human mother, Human father) {
        super(name, surname, year, mother, father);
    }

    public Woman(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    @Override
    public Human bornChild() {
        String [] names = new String[]{"Nicat", "Togrul", "Remzi"};
        Random random = new Random();
        int rand = random.nextInt(names.length);
        Human child;
        int randint = random.nextInt(2);
        if (randint == 0){
            child = new Woman(names[randint],this.getSurname(), 2001, (this.getFamily().getFather().getIq() + this.getIq()/2),new String[][]{});
        }
        else {
            child = new Man(names[randint],this.getSurname(), 2001, (this.getFamily().getFather().getIq() + this.getIq()/2),new String[][]{});
        }
        this.getFamily().addChild(child);
        return child;
    }

}
