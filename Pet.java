public  abstract class Pet {
    private Spacies species;
    private String nickName;
    private int age;
    private int trickLevel;
    private String[] habits;

    static {
        System.out.println(Pet.class.getName() + " class is loaded");
    }

    {
        System.out.println(Pet.class.getName() + " object is created");
    }



    public Pet(Spacies species, String nickName) {
        this.species = species;
        this.nickName = nickName;
    }

    public Pet(Spacies species, String nickName, int age, int trickLevel, String[] habits) {
        if (species == null){
            this.species = Spacies.UNKNOWN;
        }
        else {
            this.species = species;
        }
        this.nickName = nickName;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public abstract void eat();
    public abstract void respond();
    public void foul() {
        System.out.println("I need to cover it up");
    }

    public String toString() {
        return species + "{nickname=" + this.nickName + ", age = " + this.age
                + ", trickLevel= " + this.trickLevel + ", habits = " + this.habits;
    }

    public void setSpecies(Spacies species) {
        this.species = species;
    }

    public Spacies getSpecies() {
        return species;

    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setAge(int age) {
        this.age = this.age;
    }

    public int getAge() {
        return age;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public String[] getHabits() {
        return habits;
    }

}
